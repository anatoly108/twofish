# coding=utf-8
import math
from bitstring import ConstBitStream, BitArray, Bits

from stuff.functions import format_for_8_bit_bin, format_for_32_bit_hex, multiply_matrices_in_galois_field, addition, \
    bitwise_xor, subtraction, multiply_matrices_in_galois_field_experiment
from stuff.matrices import m_two, table_q_zero, table_q_one, m_one, m_one_dec, init_vector


class TwofishEncoding:
    def __init__(self):
        pass

    KEY_SIZE = 128
    J_SIZE = 8
    INIT_VECTOR_LEN = 128

    s_matrix = None

    s_boxes = [None]*4

    __key = BitArray()

    extended_key = []

    _last_to_save = None
    last_available_number = 0

    def __multiply_m_one(self, vector):
        g_vector = multiply_matrices_in_galois_field_experiment(vector, m_one_dec)[0]
        return g_vector

    def __divide_m_one(self, vector):
        # s_vector = multiply_matrices_in_galois_field(vector, m_one_reversed, 16, 16)
        pass


    def encode_block(self, block_128_bit):
        a_block = BitArray(block_128_bit[0:32])
        b_block = BitArray(block_128_bit[32:64])
        c_block = BitArray(block_128_bit[64:96])
        d_block = BitArray(block_128_bit[96:128])

        a_block = bitwise_xor(a_block, self.extended_key[0])
        b_block = bitwise_xor(b_block, self.extended_key[1])
        c_block = bitwise_xor(c_block, self.extended_key[2])
        d_block = bitwise_xor(d_block, self.extended_key[3])

        for round_index in range(0, 16):

            # 1.Содержимое субблока B циклически сдвигается влево на 8 бит.
            b_block.rol(8)

            # 2.Субблок A обрабатывается операцией g(), которая будет подробно описана ниже.
            a_block = self.g_function(a_block)

            # 3.Субблок B также обрабатывается операцией g().
            b_block = self.g_function(b_block)

            # 4.Субблок B накладывается на A с помощью сложения по модулю 232, после чего аналогичным образом выполняется наложение субблока A на субблок B.
            a_block = addition(a_block, b_block)

            # 5.Фрагмент расширенного ключа K2r+8 (где r — номер текущего раунда, начиная с 0) складывается с субблоком A по модулю 232.
            key_index = 2 * round_index + 8
            subkey = self.extended_key[key_index]
            a_block = addition(a_block, subkey)

            # 6.Аналогично предыдущему шагу, K2r+9 накладывается на субблок B.
            key_index = 2 * round_index + 9
            subkey = self.extended_key[key_index]
            b_block = addition(b_block, subkey)

            # 7.Субблок A накладывается на C операцией XOR.
            c_block = bitwise_xor(c_block, a_block)

            # 8.Содержимое субблока D циклически сдвигается влево на 1 бит.
            d_block.rol(1)

            # 9.Субблок B накладывается на D операцией XOR.
            d_block = bitwise_xor(d_block, b_block)

            # 10.Содержимое субблока C циклически сдвигается вправо на 1 бит.
            c_block.ror(1)

            # В конце каждого раунда, за исключением последнего, субблоки A (значение до описанной выше обработки) и C меняются местами; субблоки B (значение до обработки) и D также
            # меняются местами.
            if round_index != 15:
                tmp = a_block
                a_block = c_block
                c_block = tmp

                tmp = b_block
                b_block = d_block
                d_block = tmp
                pass

        a_block = bitwise_xor(a_block, self.extended_key[4])
        b_block = bitwise_xor(b_block, self.extended_key[5])
        c_block = bitwise_xor(c_block, self.extended_key[6])
        d_block = bitwise_xor(d_block, self.extended_key[7])

        encoded_block = a_block + b_block + c_block + d_block
        return encoded_block

    def decode_block(self, block_128_bit):
        a_block = BitArray(block_128_bit[0:32])
        b_block = BitArray(block_128_bit[32:64])
        c_block = BitArray(block_128_bit[64:96])
        d_block = BitArray(block_128_bit[96:128])

        a_block = bitwise_xor(a_block, self.extended_key[4])
        b_block = bitwise_xor(b_block, self.extended_key[5])
        c_block = bitwise_xor(c_block, self.extended_key[6])
        d_block = bitwise_xor(d_block, self.extended_key[7])

        for round_index in xrange(15, -1, -1):
            # В конце каждого раунда, за исключением последнего, субблоки A (значение до описанной выше обработки) и C меняются местами; субблоки B (значение до обработки) и D также
            # меняются местами.
            if round_index != 15:
                tmp = a_block
                a_block = c_block
                c_block = tmp

                tmp = b_block
                b_block = d_block
                d_block = tmp

            # 10.Содержимое субблока C циклически сдвигается вправо на 1 бит.
            c_block.rol(1)

            # 9.Субблок B накладывается на D операцией XOR.
            d_block = bitwise_xor(d_block, b_block)

            # 8.Содержимое субблока D циклически сдвигается влево на 1 бит.
            d_block.ror(1)

            # 7.Субблок A накладывается на C операцией XOR.
            c_block = bitwise_xor(c_block, a_block)

            # 6.Аналогично предыдущему шагу, K2r+9 накладывается на субблок B.
            key_index = 2 * round_index + 9
            subkey = self.extended_key[key_index]
            b_block = subtraction(b_block, subkey)

            # 5.Фрагмент расширенного ключа K2r+8 (где r — номер текущего раунда, начиная с 0) складывается с субблоком A по модулю 232.
            key_index = 2 * round_index + 8
            subkey = self.extended_key[key_index]
            a_block = subtraction(a_block, subkey)

            # 4.Субблок B накладывается на A с помощью сложения по модулю 232, после чего аналогичным образом выполняется наложение субблока A на субблок B.
            a_block = subtraction(a_block, b_block)

            # 3.Субблок B также обрабатывается операцией g().
            b_block = self.g_decode_function(b_block)

            # 2.Субблок A обрабатывается операцией g(), которая будет подробно описана ниже.
            a_block = self.g_decode_function(a_block)

            # 1.Содержимое субблока B циклически сдвигается влево на 8 бит.
            b_block.ror(8)

        a_block = bitwise_xor(a_block, self.extended_key[0])
        b_block = bitwise_xor(b_block, self.extended_key[1])
        c_block = bitwise_xor(c_block, self.extended_key[2])
        d_block = bitwise_xor(d_block, self.extended_key[3])

        encoded_block = a_block + b_block + c_block + d_block
        return encoded_block

    def s_box(self, block_8_bit, index):
        replaced = self.s_boxes[index][block_8_bit.uint]
        bit_array = BitArray(format_for_8_bit_bin(replaced))
        return bit_array

    def s_box_reversed(self, block_8_bit, index):
        replaced = self.s_boxes[index].index(block_8_bit.uint)
        bit_array = BitArray(format_for_8_bit_bin(replaced))
        return bit_array

    def g_function(self, block):
        first = block[0:8]
        second = block[8:16]
        third = block[16:24]
        fourth = block[24:32]

        s0 = self.s_box(first, 0)
        s1 = self.s_box(second, 1)
        s2 = self.s_box(third, 2)
        s3 = self.s_box(fourth, 3)

        s_vector = [[s0.uint, s1.uint, s2.uint, s3.uint]]
        g_vector = s_vector[0]
        #self.__multiply_m_one(s_vector)
        bin1 = BitArray(format_for_8_bit_bin(g_vector[0]))
        bin2 = BitArray(format_for_8_bit_bin(g_vector[1]))
        bin3 = BitArray(format_for_8_bit_bin(g_vector[2]))
        bin4 = BitArray(format_for_8_bit_bin(g_vector[3]))

        result_str = bin1 + bin2 + bin3 + bin4
        result = BitArray(result_str)

        return result

    def g_decode_function(self, block):
        first = block[0:8]
        second = block[8:16]
        third = block[16:24]
        fourth = block[24:32]

        g_vector = [[first, second, third, fourth]]
        # s_vector = multiply_matrices_in_galois_field_experiment(g_vector, m_one_dec)
        s_vector = g_vector[0]

        s0 = self.s_box_reversed(s_vector[0], 0)
        s1 = self.s_box_reversed(s_vector[1], 1)
        s2 = self.s_box_reversed(s_vector[2], 2)
        s3 = self.s_box_reversed(s_vector[3], 3)

        result = s0 + s1 + s2 + s3
        return result

    def q_function(self, q_type, fragment_8_bit):
        a_zero = fragment_8_bit.uint / 16
        b_zero = fragment_8_bit.uint % 16
        table = None
        if q_type == 0:
            table = table_q_zero
        elif q_type == 1:
            table = table_q_one

        # a1 - ниббла (но у меня она тут в 10-ной СС)
        a_1 = a_zero ^ b_zero
        b_zero_bits = BitArray(format_for_8_bit_bin(b_zero))
        b_zero_bits.ror(4)
        b_1_1 = a_zero ^ b_zero_bits.uint
        b_1_2 = b_1_1 ^ (8 * a_zero)
        b_1 = b_1_2 % 16

        if a_1 > 15:
            pass
        a_2 = int(table[0][a_1], 16)
        b_2 = int(table[1][b_1], 16)

        a_3 = a_2 ^ b_2
        b_2_bits = BitArray(format_for_8_bit_bin(b_2))
        b_2_bits.ror(4)
        b_3_1 = a_2 ^ b_2_bits.uint
        b_3_2 = b_3_1 ^ (8 * a_2)
        b_3 = b_3_2 % 16

        a_4 = int(table[2][a_3], 16)
        b_4 = int(table[3][b_3], 16)

        y = 16 * b_4 + a_4

        y_bits = BitArray(format_for_8_bit_bin(y))
        return y_bits

    def h_function(self, param_32_bit, m_by_k_by_32_bit, is_encrypt):
        frag1 = BitArray(param_32_bit[0:8])
        frag2 = BitArray(param_32_bit[8:16])
        frag3 = BitArray(param_32_bit[16:24])
        frag4 = BitArray(param_32_bit[24:32])

        # Шаг 3
        frag1 = self.q_function(1, frag1)
        frag2 = self.q_function(0, frag2)
        frag3 = self.q_function(1, frag3)
        frag4 = self.q_function(0, frag4)

        result1_word = frag1 + frag2 + frag3 + frag4
        result1 = addition(result1_word, m_by_k_by_32_bit[1])

        # Шаг 4
        frag1 = result1[0:8]
        frag2 = result1[8:16]
        frag3 = result1[16:24]
        frag4 = result1[24:32]

        frag1 = self.q_function(1, frag1)
        frag2 = self.q_function(1, frag2)
        frag3 = self.q_function(0, frag3)
        frag4 = self.q_function(0, frag4)

        result2_word = frag1 + frag2 + frag3 + frag4
        result2 = addition(result2_word, m_by_k_by_32_bit[0])

        # Шаг 5
        frag1 = result2[0:8]
        frag2 = result2[8:16]
        frag3 = result2[16:24]
        frag4 = result2[24:32]

        frag1 = self.q_function(0, frag1)
        frag2 = self.q_function(1, frag2)
        frag3 = self.q_function(0, frag3)
        frag4 = self.q_function(1, frag4)

        frags = [[frag1.uint, frag2.uint, frag3.uint, frag4.uint]]

        result_step_5 = self.__multiply_m_one(frags)

        H = 0
        for i in xrange(0, 4):
            H += result_step_5[i] * 2 ** (8 * i)

        hex_h = format_for_32_bit_hex(H)
        result = BitArray(hex_h)
        return result

    def key_preprocessing(self, key):
        key = BitArray(key)

        ro = 16843009
        M_0 = BitArray(key[0:32])
        M_1 = BitArray(key[32:64])
        M_2 = BitArray(key[64:96])
        M_3 = BitArray( key[96:128])

        self.__key = M_0 + M_1 + M_2 + M_3

        G1 = []
        G2 = []

        for i in xrange(0, 64, 8):
            data = BitArray(key[i:i + 8])
            G1.append(data.hex)
        for i in xrange(64, 128, 8):
            data = BitArray(key[i:i + 8])
            G2.append(data.hex)

        transposed = zip(*m_two)
        S1 = multiply_matrices_in_galois_field([G1], transposed, 16, 16)
        S2 = multiply_matrices_in_galois_field([G2], transposed, 16, 16)
        s1_bin = format_for_8_bit_bin(S1[0][0]) + format_for_8_bit_bin(S1[0][1]) + format_for_8_bit_bin(
            S1[0][2]) + format_for_8_bit_bin(S1[0][3])
        s2_bin = format_for_8_bit_bin(S2[0][0]) + format_for_8_bit_bin(S2[0][1]) + format_for_8_bit_bin(
            S2[0][2]) + format_for_8_bit_bin(S2[0][3])

        s1_bitarray = BitArray(s1_bin)
        s2_bitarray = BitArray(s2_bin)

        self.s_matrix = [s1_bitarray, s2_bitarray]

        M_e = [M_0, M_2]
        M_oo = [M_1, M_3]
        key_fragments_num = 40
        A = [None] * key_fragments_num
        B = [None] * key_fragments_num

        k = [None] * key_fragments_num

        for i in xrange(0, key_fragments_num):
            param_32_for_a = 2 * i * ro
            param_32_for_a = format_for_32_bit_hex(param_32_for_a)
            param_32_for_a = BitArray(param_32_for_a)
            A[i] = self.h_function(param_32_for_a, M_e, True)

            param_32_for_b = ((2 * i) + 1) * ro
            param_32_for_b = format_for_32_bit_hex(param_32_for_b)
            param_32_for_b = BitArray(param_32_for_b)
            result_b = self.h_function(param_32_for_b, M_oo, True)
            result_b.rol(8)
            B[i] = result_b

        for i in xrange(0, key_fragments_num, 2):
            k[i] = addition(A[i], B[i])
        for i in xrange(1, key_fragments_num, 2):
            b_mul_2 = 2 * B[i].uint
            b_hex = format_for_32_bit_hex(b_mul_2)
            b_mul_2_bit_array = BitArray(b_hex)
            add_result = addition(A[i], b_mul_2_bit_array)
            add_result.rol(9)
            k[i] = add_result
        self.extended_key = k

    def initialize_s_boxes(self):
        s_0_0 = self.s_matrix[0][0:8]
        s_0_1 = self.s_matrix[0][8:16]
        s_0_2 = self.s_matrix[0][16:24]
        s_0_3 = self.s_matrix[0][24:32]

        s_1_0 = self.s_matrix[1][0:8]
        s_1_1 = self.s_matrix[1][8:16]
        s_1_2 = self.s_matrix[1][16:24]
        s_1_3 = self.s_matrix[1][24:32]

        self.s_boxes[0] = []
        self.s_boxes[1] = []
        self.s_boxes[2] = []
        self.s_boxes[3] = []
        box0 = self.s_boxes[0]
        box1 = self.s_boxes[1]
        box2 = self.s_boxes[2]
        box3 = self.s_boxes[3]

        for i in xrange(0, 256):
            block_8_bit = BitArray(format_for_8_bit_bin(i))

            # box1
            sb_1_1 = self.q_function(0, block_8_bit) ^ s_0_0
            sb_1_2 = self.q_function(0, sb_1_1) ^ s_1_0
            sb_1_result = self.q_function(1, sb_1_2)
            box0.append(sb_1_result.uint)

            # box2
            sb_2_1 = self.q_function(1, block_8_bit) ^ s_0_1
            sb_2_2 = self.q_function(0, sb_2_1) ^ s_1_1
            sb_2_result = self.q_function(1, sb_2_2)
            box1.append(sb_2_result.uint)

            #box3
            sb_3_1 = self.q_function(0, block_8_bit) ^ s_0_2
            sb_3_2 = self.q_function(1, sb_3_1) ^ s_1_2
            sb_3_result = self.q_function(1, sb_3_2)
            box2.append(sb_3_result.uint)

            #box4
            sb_4_1 = self.q_function(1, block_8_bit) ^ s_0_3
            sb_4_2 = self.q_function(1, sb_4_1) ^ s_1_3
            sb_4_result = self.q_function(0, sb_4_2)
            box3.append(sb_4_result.uint)

    def encode_file(self, file_path, encoded_file_path):
        stream = ConstBitStream(filename=file_path)
        file_stream = open(encoded_file_path, 'wb')
        until = int(math.ceil(len(stream) / 128.0))
        last_available = (until * 128) - stream.len
        last_iteration = until - 1
        last_to_read = 128 - last_available
        for i in xrange(0, until):
            if i == last_iteration:
                to_read = last_to_read
            else:
                to_read = 128
            block = stream.read(to_read)
            if i == last_iteration:
                block_str = str(block.bin)
                for zero_i in xrange(0, last_available):
                    block_str += '0'
                block_str = '0b' + block_str
                block = ConstBitStream(block_str)
            handled_bits = self.encode_block(block)
            if i == last_iteration:
                last = BitArray(format_for_8_bit_bin(last_available))
                handled_bits = handled_bits + last
            array = bytearray(handled_bits.bytes)
            file_stream.write(array)
        file_stream.close()

    def decode_file(self, file_path, encoded_file_path):
        stream = ConstBitStream(filename=file_path)
        file_stream = open(encoded_file_path, 'wb')
        until = int(math.ceil(len(stream) / 128.0))
        last_iteration = until - 1
        for i in xrange(0, until):
            to_read = 128

            if i == last_iteration:
                to_read = 8

            block = stream.read(to_read)

            if i == last_iteration:
                self.last_available_number = block.uint
            else:
                handled_bits = self.decode_block(block)

            if i == (last_iteration - 1):
                self._last_to_save = handled_bits
                continue

            if i == last_iteration:
                # self.last_available_number += 1
                number = 128 - self.last_available_number
                handled_bits = handled_bits[0:number]

            bytes_array = bytearray(handled_bits.bytes)
            file_stream.write(bytes_array)
        file_stream.close()

    def encode_cfb(self, file_path, encoded_file_path):
        stream = ConstBitStream(filename=file_path)
        file_stream = open(encoded_file_path, 'wb')
        until = len(stream) / self.J_SIZE
        block = init_vector
        upper = self.INIT_VECTOR_LEN - self.J_SIZE
        for i in xrange(0,until):
            block = self.encode_block(block)
            part = block[0:self.J_SIZE]
            original = stream.read(self.J_SIZE)
            xored = bitwise_xor(part, original)
            array = bytearray(xored.bytes)
            file_stream.write(array)

            block.rol(self.J_SIZE)
            block = block[0:upper] + xored

        file_stream.close()

    def decode_cfb(self,file_path, encoded_file_path):
        stream = ConstBitStream(filename=file_path)
        file_stream = open(encoded_file_path, 'wb')
        until = len(stream) / self.J_SIZE
        block = init_vector
        upper = self.INIT_VECTOR_LEN - self.J_SIZE
        for i in xrange(0,until):
            block = self.encode_block(block)
            readed = stream.read(self.J_SIZE)
            part = block[0:self.J_SIZE]
            xored = bitwise_xor(part, readed)
            array = bytearray(xored.bytes)
            file_stream.write(array)

            block.rol(self.J_SIZE)
            block = block[0:upper] + readed

        file_stream.close()