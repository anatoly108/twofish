# coding=utf-8
from bitstring import BitArray
from stuff import Gf2

__author__ = 'Tolya'


def format_for_8_bit_bin(number):
    return format(number, '#010b')

def format_for_64_bit_bin(number):
    return format(number, '#066b')

def format_for_32_bit_hex(number):
    return format(number, '#010x')


def multiply_matrices_in_galois_field(matrix1, matrix2, base1, base2):
    # Считаем с расчётом на то, что у matrix1 все строки одинакового размера
    # Ещё особенность: вектор должен быть объявлен как матрица: [[ a, b, c ]]
    # И тип возвращаемых элементов - инт
    # base1 и 2 - системы счисления элементов матриц. Результат в 10-ричной
    len1 = len(matrix1[0])
    len2 = len(matrix2)
    if len1 != len2:
        raise Exception('Matrices are different sizes')

    result_width = len(matrix1)
    result_height = len(matrix2[0])
    result = [[0 for x in xrange(result_height)] for x in xrange(result_width)]

    for i in xrange(0, result_width):
        for j in xrange(0, result_height):
            element_1 = int(matrix1[i][j], base1)
            element_2 = int(matrix2[j][i], base2)
            new_element = Gf2.gmul(element_1, element_2)
            result[i][j] = new_element
    return result

def multiply_matrices_in_galois_field_experiment(matrix1, matrix2):
    len1 = len(matrix1[0])
    len2 = len(matrix2)
    if len1 != len2:
        raise Exception('Matrices are different sizes')

    result_width = len(matrix1)
    result_height = len(matrix2[0])
    result = [[0 for x in xrange(result_height)] for x in xrange(result_width)]

    for i in xrange(0, result_width):
        for j in xrange(0, result_height):
            element_1 = matrix1[i][j]
            element_2 = matrix2[j][i]
            new_element = Gf2.gmul(element_1, element_2)
            result[i][j] = new_element
    return result

def addition(block1, block2):
    result_int = (block1.uint + block2.uint) % 4294967296
    result_hex = format_for_32_bit_hex(result_int)
    result = BitArray(result_hex)
    return result

def subtraction(block1, block2):
    result_int = (block1.uint - block2.uint) % 4294967296
    result_hex = format_for_32_bit_hex(result_int)
    result = BitArray(result_hex)
    return result


def bitwise_xor(block1, block2):
    result = BitArray([None] * len(block1))
    for i in range(0, len(block1)):
        result[i] = block1[i] ^ block2[i]
    return result