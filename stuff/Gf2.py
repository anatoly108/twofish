def __getpoly():
    first = 6
    second = 5
    third = 3
    return (1 << first) + (1 << second) + (1 << third) + 1


def gmul(a, b):
    deg = 8
    poly = __getpoly()
    max_1 = (1 << deg) - 1
    max_2 = 1 << (deg - 1)
    z = 0
    if a & 1:
        r = b
    else:
        r = 0
    for i in xrange(1, deg + 1):
        mark = b & max_2
        b = (b << 1) & max_1
        if mark:  # Note: not really protected by timing attack. since...
            b ^= poly
        else:
            b ^= z
        # print a & (1 << i), r, b
        if a & (1 << i):
            r ^= b
        else:
            r ^= z
    return r
