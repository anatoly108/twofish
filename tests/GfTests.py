from stuff import Gf2

__author__ = 'Tolya'

import unittest


class MyTestCase(unittest.TestCase):
    def test_something(self):
        result = Gf2.gmul(1000, 2)
        self.assertEqual(result, 107)


if __name__ == '__main__':
    unittest.main()
