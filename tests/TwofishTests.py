# coding=utf-8
from collections import Counter
import random
import sys

from bitstring import BitArray

from stuff.Twofish import TwofishEncoding
from stuff.functions import format_for_8_bit_bin


__author__ = 'Tolya'

import unittest


class TwofishTests(unittest.TestCase):
    def test_gen_rand_key(self):
        for i in xrange(0, 128):
            sys.stdout.write(str(random.randint(0, 1)))

    def test_symmetric(self):
        key = '01111011110001010010011110011001011001100111110100011101010010100110110111110111101010000100001000001101101010101111001100100011'
        test_block = BitArray('0x0123456789ABCDEF0123456789ABCDEF')
        target = TwofishEncoding()
        target.key_preprocessing(key)
        target.initialize_s_boxes()
        encode_result = target.encode_block(test_block)
        decode_result = target.decode_block(encode_result)
        assert test_block == decode_result

    def test_q_duplicates_for_different_input(self):
        target = TwofishEncoding()
        result = []
        for fragment in xrange(0, 255):
            bit_array = BitArray(format_for_8_bit_bin(fragment))
            result.append(target.q_function(0, bit_array).uint)
        duplicates = [k for k,v in Counter(result).items() if v>1]
        print(duplicates)
        assert len(duplicates) == 0

    def test_s_box_symmetric(self):
        key = '01111011110001010010011110011001011001100111110100011101010010100110110111110111101010000100001000001101101010101111001100100011'
        test_block = BitArray('0b01101110')
        target = TwofishEncoding()
        target.key_preprocessing(key)
        target.initialize_s_boxes()
        handled = target.s_box(test_block, 0)
        result = target.s_box_reversed(handled, 0)

        assert result == test_block

    def test_g_symmetric(self):
        key = '01111011110001010010011110011001011001100111110100011101010010100110110111110111101010000100001000001101101010101111001100100011'
        test_block = BitArray('0b11011101110111011101110111011101')
        target = TwofishEncoding()
        target.key_preprocessing(key)
        target.initialize_s_boxes()
        encode_result = target.g_function(test_block)
        decode_result = target.g_decode_function(encode_result)
        assert test_block == decode_result

    def test_some_g_exp(self):
        test_block = BitArray('0b11011101110111011101110111011101')
        ar = test_block.bytes
        pass
    if __name__ == '__main__':
        unittest.main()
