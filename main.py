# coding=utf-8
from _lsprof import profiler_entry
from bitstring import BitArray, ConstBitStream
from homework.md5 import md5_hash
from homework.rsa import rsa_keys, rsa_encode, rsa_decode
from stuff.Twofish import TwofishEncoding
from tests.TestData import twofish_test_extended_key, twofish_test_s_boxes, twofish_test_s_matrix
import sys


def main():
    # twofish_encoding = TwofishEncoding()
    # twofish_encoding.s_boxes = twofish_test_s_boxes
    # twofish_encoding.extended_key = twofish_test_extended_key
    # twofish_encoding.s_matrix = twofish_test_s_matrix
    # # twofish_encoding.encode_cfb('test_file', 'cfb_encoded')
    # twofish_encoding.decode_cfb('cfb_encoded', 'cfb_decoded')
    #
    # sys.exit()

    # keys = rsa_keys()
    # message = '12'
    # open = [3, 9173503]
    # close = [6111579, 9173503]
    # keys = [open, close]
    # encoded = rsa_encode(message, keys[0])
    # print(encoded)
    # decoded = rsa_decode(encoded, keys[1])
    # print(decoded)
    #
    # sys.exit()
    while True:
        input = raw_input('Enter command: ')
        if input == 'exit':
            sys.exit()
        splitted = input.split(' ')
        if len(splitted) != 2:
            continue
        command = splitted[0]
        param = splitted[1]
        if command == 'encrypt':
            twofish_encoding = TwofishEncoding()
            key = raw_input('Enter 128bit key: ')
            twofish_encoding.key_preprocessing(key)
            twofish_encoding.initialize_s_boxes()
            save_path = param + '.encoded'
            twofish_encoding.encode_file(param, save_path)
            print 'ok, saved to ' + save_path

        elif command == 'encrypt_cfb':
            twofish_encoding = TwofishEncoding()
            key = raw_input('Enter 128bit key: ')
            twofish_encoding.key_preprocessing(key)
            twofish_encoding.initialize_s_boxes()
            save_path = param + '.encoded'
            twofish_encoding.encode_cfb(param, save_path)
            print 'ok, saved to ' + save_path

        elif command == 'decrypt_cfb':
            twofish_encoding = TwofishEncoding()
            key = raw_input('Enter 128bit key: ')
            twofish_encoding.key_preprocessing(key)
            twofish_encoding.initialize_s_boxes()
            save_path = param + '.decoded'
            twofish_encoding.decode_cfb(param, save_path)
            print 'ok, saved to ' + save_path

        elif command == 'decrypt':
            twofish_encoding = TwofishEncoding()
            key = raw_input('Enter 128bit key: ')
            if (len(key) != 128) & (len(key) != 34):
                continue
            twofish_encoding.key_preprocessing(key)
            twofish_encoding.initialize_s_boxes()
            save_path = param + '.decoded'
            twofish_encoding.decode_file(param, save_path)
            print 'ok, saved to ' + save_path

        elif command == 'md5':
            # bits = '0b' + ''.join(format(x, 'b') for x in bytearray(param))
            # bit_array = BitArray(bits)
            bits = ConstBitStream(filename=param)
            print('started')
            hash = md5_hash(bits)
            print(hash)


def md5_launch():
    str = 'Дух музыки'
    bits = '0b' + ''.join(format(x, 'b') for x in bytearray(str))
    bit_array = BitArray(bits)
    hash = md5_hash(bit_array)

    print(hash)

    #TODO: Передача файла, ГУИ


if __name__ == "__main__":
    main()