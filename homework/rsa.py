from bitstring import BitArray
import random
__author__ = 'Tolya'


def rsa_keys():
    p = 6491#rand_prime()
    q = 6521#rand_prime()
    n = p * q
    fin = (p - 1) * (q - 1)
    e = 3
    d = 0
    while ((d*e) % fin) != 1:
        d += 1

    open = [e, n]
    close = [d, n]
    print('open: ' + open.__str__())
    print('close: ' + close.__str__())

    return [open, close]

def rsa_encode(message, open):
    bits = ''.join('{:08b}'.format(ord(c)) for c in message)
    bits = '0b' + bits
    message_bit_array = BitArray(bits)
    e = open[0]
    n = open[1]
    c = pow(message_bit_array.uint, e, n)
    bits = BitArray(bin(c))
    return bits

def rsa_decode(message_bit_array, close):
    d = close[0]
    n = close[1]
    m = pow(message_bit_array.uint, d, n)
    bits = bin(m)
    bits = '0' + bits[2:len(bits)]
    str = ''.join(chr(int(bits[i:i+8], 2)) for i in xrange(0, len(bits), 8))
    return str


def frombits(bits):
    bits = bits[2:len(bits)]
    str = ''.join(chr(int(bits[i:i+8], 2)) for i in xrange(0, len(bits), 8))
    return str

def rand_prime():
    while True:
        p = random.randrange(10001, 100000, 2)
        if all(p % n != 0 for n in range(3, int((p ** 0.5) + 1), 2)):
            return p