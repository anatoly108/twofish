from math import sin
from bitstring import BitArray
import math
from stuff.functions import format_for_64_bit_bin, format_for_32_bit_hex

__author__ = 'Tolya'

__T = [None] * 64
s = [7, 12, 17, 22, 7, 12, 17, 22, 7, 12, 17, 22, 7, 12, 17, 22, 5, 9, 14, 20, 5, 9, 14, 20, 5, 9, 14, 20, 5, 9, 14, 20,
     4, 11, 16, 23, 4, 11, 16, 23, 4, 11, 16, 23, 4, 11, 16, 23,
     6, 10, 15, 21, 6, 10, 15, 21, 6, 10, 15, 21, 6, 10, 15, 21]


def condition(bit_array):
    if (len(bit_array) % 512) == 448:
        return True
    else:
        return False


def F(X, Y, Z):
    y1 = X & Y
    not_x = ~X
    y2 = not_x & Z
    result = y1 | y2
    return result


def G(X, Y, Z):
    result = (X & Z) | (Y & (~Z))
    return result


def H(X, Y, Z):
    result = X ^ Y ^ Z
    return result


def I(X, Y, Z):
    result = Y ^ (X | (~Z))
    return result


def md5_hash(bit_array):
    bit_array += BitArray('0b1')
    b = len(bit_array) % 18446744073709551616
    b_64 = BitArray(format_for_64_bit_bin(b))
    low = b_64[0:32]
    high = b_64[32:64]
    while not condition(bit_array):
        bit_array += BitArray('0b0')

    bit_array += low
    bit_array += high

    A0 = BitArray('0x67452301')
    B0 = BitArray('0xEFCDAB89')
    C0 = BitArray('0x98BADCFE')
    D0 = BitArray('0x10325476')

    for i in xrange(0, 64):
        result = 4294967296 * abs(sin(i))
        dec, int_part = math.modf(result)
        __T[i] = int_part

    until = len(bit_array) / 512

    for i in xrange(0, until):
        lowest = i * 512
        highest = lowest + 512
        chunk = bit_array[lowest:highest]
        M = [None] * 16
        for j in xrange(0, 16):
            low = j * 32
            upper = low + 32
            M[j] = chunk[low:upper]

        A = A0
        B = B0
        C = C0
        D = D0
        g = 0
        Func = 0
        for j in xrange(0, 64):
            if 0 <= j <= 15:
                Func = F(B, C, D)
                g = j
            elif 16 <= j <= 31:
                Func = G(B, C, D)
                g = (5 * j + 1) % 16
            elif 32 <= j <= 47:
                Func = H(B, C, D)
                g = (3 * j + 5) % 16
            elif 48 <= j <= 63:
                Func = I(B, C, D)
                g = (7 * j) % 16

            rotate_to = s[j]
            t = __T[j]
            m = M[g].uint
            expression = int(A.uint + Func.uint + t + m) % 4294967296
            expression = BitArray(format_for_32_bit_hex(expression))
            expression.rol(rotate_to)
            A = (B.uint + expression.uint) % 4294967296
            A = BitArray(format_for_32_bit_hex(A))

            dTemp = D
            D = C
            C = B
            B = A
            A = dTemp

            add_res1 = (A0.uint + A.uint) % 4294967296
            A0 = BitArray(format_for_32_bit_hex(add_res1))

            add_res2 = (B0.uint + B.uint) % 4294967296
            B0 = BitArray(format_for_32_bit_hex(add_res2))

            add_res3 = (C0.uint + C.uint) % 4294967296
            C0 = BitArray(format_for_32_bit_hex(add_res3))

            add_res4 = (D0.uint + D.uint) % 4294967296
            D0 = BitArray(format_for_32_bit_hex(add_res4))

    output = A0 + B0 + C0 + D0

    return output
